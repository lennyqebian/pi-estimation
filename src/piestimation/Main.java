package piestimation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.Point2D;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author leonardoquevedo
 */
public class Main extends javax.swing.JFrame {

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
    }

    private void plotPoints(ArrayList<Point2D.Double> points) {
        ArrayList<Point2D.Double> shape, 
                circle = new ArrayList(), 
                square = new ArrayList();
        
        for (Point2D.Double point : points) {
            if (Math.pow(point.getX(), 2) + Math.pow(point.getY(), 2) <= 1) {
                shape = circle;
            } else {
                shape = square;
            }
            
            shape.add(point);
            shape.add(new Point2D.Double(-point.getX(), point.getY()));
            shape.add(new Point2D.Double(-point.getX(), -point.getY()));
            shape.add(new Point2D.Double(point.getX(), -point.getY()));
        }
        
        ((GraphPanel) this.pnlGraph).setPoints(square, circle);
        this.pnlGraph.repaint();
    }

    public class Random {

        protected int[] _seed;

        public Random() {
            this.seed();
        }

        private void seed() {
            long a = (long) (System.currentTimeMillis() / 1000L * 256);
            long x = a % 30268;
            long y = a % 30306;
            long z = a % 30322;
            this._seed = new int[]{(int) x + 1, (int) y + 1, (int) z + 1};
        }

        public double random() {
            int x = this._seed[0];
            int y = this._seed[1];
            int z = this._seed[2];

            this._seed[0] = x = (171 * x) % 30269;
            this._seed[1] = y = (172 * y) % 30307;
            this._seed[2] = z = (170 * z) % 30323;

            return (x / 30269.0 + y / 30307.0 + z / 30323.0) % 1.0;
        }

        public ArrayList<Point2D.Double> randomPoints(int n) throws Exception {
            if (!(n > 0)) {
                throw new Exception("¡La cantidad de puntos aleatorios a generar debe ser mayor que cero (0)!");
            }

            ArrayList<Point2D.Double> U = new ArrayList();

            for (int i = 0; i < n; i++) {
                Point2D.Double point = new Point2D.Double(this.random(), this.random());
                U.add(point);
            }
            return U;
        }
    }
    
    public static class World {
        public static String formatPoints(ArrayList<Point2D.Double> points) {
            ArrayList<String> lines = new ArrayList();
            int decimalPlaces = 7;
            String format = "%." + decimalPlaces + "f";

            points.forEach((point) -> {
                lines.add("(" + String.format(format, point.getX()) + ", " 
                        + String.format(format, point.getY()) + ")");
            });
            return String.join(", \n", lines);
        }
        
        public static String getResponse(ArrayList<Point2D.Double> points) {
            ArrayList<Point2D.Double> pointsInside = new ArrayList();

            points.stream().filter((point) -> (Math.pow(point.getX(), 2) + Math.pow(point.getY(), 2) <= 1)).forEachOrdered((point) -> {
                pointsInside.add(point);
            });

            double percentageInside = (double) pointsInside.size() / points.size() * 100;
            double piEstimation = 4 * percentageInside / 100;
            double diff = Math.abs(piEstimation - Math.PI);
            String response = ""
                    + "Estimación de Pi (π): " + piEstimation + "\n"
                    + "Valor Real de Pi (π): " + Math.PI + "\n"
                    + "Diferencia: " + diff;

            if (diff < 0.1) {
                response += "\n" + "La estimación de Pi (π) es cercana al valor real";
            }
            return response;
        }
    }
    
    public class GraphPanel extends JPanel {
        protected ArrayList<Point2D.Double> square;
        protected ArrayList<Point2D.Double> circle;
        
        public void setPoints(ArrayList<Point2D.Double> square, ArrayList<Point2D.Double> circle) {
            this.square = square;
            this.circle = circle;
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            Graphics2D g2d = (Graphics2D) g;
            int x, y;
            g2d.drawLine(this.getWidth() / 2, 0, this.getWidth() / 2, this.getHeight());
            g2d.drawLine(0, this.getHeight() / 2, this.getWidth(), this.getHeight() / 2);
            g2d.setStroke(new BasicStroke(1.5f));
            g2d.drawRect(20, 20, 200, 200);
            g2d.setColor(Color.BLUE);
            g2d.drawOval(20, 20, 200, 200);
            
            if (this.square != null) {
                g2d.setColor(Color.magenta);
                
                for (Point2D.Double point : this.square) {
                    x = (int) Math.floor(100 * point.getX()) + (this.getWidth() / 2);
                    y = (int) Math.floor(100 * point.getY()) + (this.getHeight()/ 2);
                    g2d.drawOval(x, y, 1, 1);
                }
            }
            
            if (this.circle != null) {
                g2d.setColor(Color.green);
                
                for (Point2D.Double point : this.circle) {
                    x = (int) Math.floor(100 * point.getX()) + (this.getWidth() / 2);
                    y = (int) Math.floor(100 * point.getY()) + (this.getHeight()/ 2);
                    g2d.drawOval(x, y, 1, 1);
                }
            }
        }
    }

    protected Random randomer;

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlInput = new javax.swing.JPanel();
        lblNPoints = new javax.swing.JLabel();
        spnNPoints = new javax.swing.JSpinner();
        lblNPointsHelp = new javax.swing.JLabel();
        btnCalculate = new javax.swing.JButton();
        pnlOutput = new javax.swing.JPanel();
        lblUPoints = new javax.swing.JLabel();
        scrlUPoints = new javax.swing.JScrollPane();
        txaUPoints = new javax.swing.JTextArea();
        pnlGraph = new GraphPanel();
        lblResponse = new javax.swing.JLabel();
        scrlResponse = new javax.swing.JScrollPane();
        txaResponse = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Estimación de Pi (π) con números pseudo aleatorios");

        pnlInput.setBorder(javax.swing.BorderFactory.createTitledBorder("Entrada"));

        lblNPoints.setText("¿Cuántos puntos aleatorios?");

        lblNPointsHelp.setFont(new java.awt.Font("Lucida Grande", 2, 13)); // NOI18N
        lblNPointsHelp.setText("<html>Entre más alto sea el número de puntos aleatorios, la estimación de Pi (π) será más precisa.</html>");

        btnCalculate.setText("Calcular");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlInputLayout = new javax.swing.GroupLayout(pnlInput);
        pnlInput.setLayout(pnlInputLayout);
        pnlInputLayout.setHorizontalGroup(
            pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNPointsHelp, javax.swing.GroupLayout.DEFAULT_SIZE, 648, Short.MAX_VALUE)
                    .addGroup(pnlInputLayout.createSequentialGroup()
                        .addComponent(lblNPoints)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(spnNPoints, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInputLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCalculate, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(166, 166, 166))
        );
        pnlInputLayout.setVerticalGroup(
            pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlInputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlInputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNPoints)
                    .addComponent(spnNPoints, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblNPointsHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCalculate)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlOutput.setBorder(javax.swing.BorderFactory.createTitledBorder("Salida"));

        lblUPoints.setText("Puntos Generados");

        txaUPoints.setEditable(false);
        txaUPoints.setColumns(12);
        txaUPoints.setLineWrap(true);
        txaUPoints.setRows(5);
        txaUPoints.setWrapStyleWord(true);
        scrlUPoints.setViewportView(txaUPoints);

        pnlGraph.setBackground(new java.awt.Color(255, 255, 216));
        pnlGraph.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout pnlGraphLayout = new javax.swing.GroupLayout(pnlGraph);
        pnlGraph.setLayout(pnlGraphLayout);
        pnlGraphLayout.setHorizontalGroup(
            pnlGraphLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 236, Short.MAX_VALUE)
        );
        pnlGraphLayout.setVerticalGroup(
            pnlGraphLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 236, Short.MAX_VALUE)
        );

        lblResponse.setText("Respuesta");

        txaResponse.setEditable(false);
        txaResponse.setColumns(20);
        txaResponse.setLineWrap(true);
        txaResponse.setRows(5);
        txaResponse.setWrapStyleWord(true);
        scrlResponse.setViewportView(txaResponse);

        javax.swing.GroupLayout pnlOutputLayout = new javax.swing.GroupLayout(pnlOutput);
        pnlOutput.setLayout(pnlOutputLayout);
        pnlOutputLayout.setHorizontalGroup(
            pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOutputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrlResponse)
                    .addGroup(pnlOutputLayout.createSequentialGroup()
                        .addGroup(pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlOutputLayout.createSequentialGroup()
                                .addComponent(scrlUPoints, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                                .addGap(67, 67, 67))
                            .addGroup(pnlOutputLayout.createSequentialGroup()
                                .addGroup(pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblResponse)
                                    .addComponent(lblUPoints))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(pnlGraph, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)))
                .addContainerGap())
        );
        pnlOutputLayout.setVerticalGroup(
            pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOutputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOutputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlOutputLayout.createSequentialGroup()
                        .addComponent(lblUPoints)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(scrlUPoints, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblResponse))
                    .addComponent(pnlGraph, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scrlResponse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlOutput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(14, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(pnlInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        this.randomer = new Random();
        int n = Integer.parseInt(this.spnNPoints.getValue().toString());

        try {
            ArrayList<Point2D.Double> points = this.randomer.randomPoints(n);
            this.txaUPoints.setText(World.formatPoints(points));
            this.txaResponse.setText(World.getResponse(points));
            this.plotPoints(points);
        } catch (Exception ex) {
            StringWriter stringWriter = new StringWriter();
            ex.printStackTrace(new PrintWriter(stringWriter));
            this.txaResponse.setText(stringWriter.toString());
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            this.showErrorDialog(ex.getMessage(), "¡Oops!");
        }
    }//GEN-LAST:event_btnCalculateActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JLabel lblNPoints;
    private javax.swing.JLabel lblNPointsHelp;
    private javax.swing.JLabel lblResponse;
    private javax.swing.JLabel lblUPoints;
    private javax.swing.JPanel pnlGraph;
    private javax.swing.JPanel pnlInput;
    private javax.swing.JPanel pnlOutput;
    private javax.swing.JScrollPane scrlResponse;
    private javax.swing.JScrollPane scrlUPoints;
    private javax.swing.JSpinner spnNPoints;
    private javax.swing.JTextArea txaResponse;
    private javax.swing.JTextArea txaUPoints;
    // End of variables declaration//GEN-END:variables

    protected void showErrorDialog(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    protected void showErrorDialog(String message) {
        this.showErrorDialog(message, "¡ERROR!");
    }

}
